"""
Functions to create output directory and extract by mask on a working directory with raster files.
"""

# Import modules
import os
import sys
import arcpy
from arcpy import env
import logging

# Check out the ArcGIS Spatial Analyst extension license.
arcpy.CheckOutExtension("Spatial")


def change_mk_dir(dir_prompt):
    """Make an output directory.
    :param dir_prompt: Filepath to existing directory or filepath with directory to create.
    :return: dir_path
    """
    dir_path = os.path.normpath(dir_prompt)

    # Check if output directory already exists, if not, create it.
    if not os.path.exists(dir_path):
        print("Creating output directory here: {}".format(dir_path))
        os.mkdir(dir_path)
        print("Changing directory to {}.".format(dir_path))
        os.chdir(dir_path)
        return dir_path
    else:
        print("Changing directory to {}.".format(dir_path))
        os.chdir(dir_path)
        return dir_path


def snap_extract(mask_feature, snap_raster, working_dir, out_dir):
    """Set snap environment and execute extract by mask.

    :param mask_feature: Feature used to ask input with (vector or raster).
    :param snap_raster: Raster layer used to snap raster to.
    :param working_dir: Directory with rasters to mask
    :param out_dir: Path to output directory. This should already exist.
    :return:
    """

    # Set Snap Raster environment
    arcpy.env.snapRaster = snap_raster
    # set workspace to working directory
    env.workspace = working_dir
    # make list rasters in workspace
    rasters = arcpy.ListRasters()
    # set counter
    counter = 1
    # loop through rasters and extract by mask (will be aligned because of the snap environment)
    for i in rasters:
        logging.info('Working on file {} of {}\n'.format(counter, len(rasters)))
        # output file
        out_path = os.path.join(out_dir, i)
        # Process: Extract by Mask
        logging.info('Masking {} with {}\nSnapping to: {}\nExporting to: {}\n\n'.format(i, mask_feature, snap_raster, out_dir))
        arcpy.gp.ExtractByMask_sa(i, mask_feature, out_path)
        # increment counter
        counter += 1




