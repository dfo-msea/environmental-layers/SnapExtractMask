﻿# snap-extract-mask

__Main author:__  Cole Fields   
__Affiliation:__  Fisheries and Oceans Canada (DFO)   
__Group:__        Marine Spatial Ecology and Analysis   
__Location:__     Institute of Ocean Sciences   
__Contact:__      e-mail: Cole.Fields@dfo-mpo.gc.ca | tel: 250-363-8060


- [Objective](#objective)
- [Summary](#summary)
- [Status](#status)
- [Contents](#contents)
- [Methods](#methods)
- [Requirements](#requirements)
- [Caveats](#caveats)
- [Uncertainty](#uncertainty)
- [Acknowledgements](#acknowledgements)
- [References](#references)


## Objective
The purpose of this script is to mask and align a set of rasters so that they have the same spatial extent and the cells
are aligned. 

## Summary
This code was developed partly to aid with species distribution modelling. Running data-driven or envelope models 
requires a set of environmental predictors, often a collection of raster surfaces that need to have the same resolution, 
extent, and whose cells are aligned. 

## Status
Completed

## Contents
run.py: This is the executable script written to be run from the command line with arguments from the user.
extract_mask_functions: This is the module with functions that are called in run.py.

## Methods
The script modifies the environment settings so that the extent is set to match the input mask feature. Next, it sets 
the snap environment to a raster file so that the output masked rasters will have their cells aligned. It creates a 
list of rasters in the working directory. It will iterate through the list of rasters, running 
`arcpy.gp.ExtractByMask_sa` to clip the raster by the mask feature (raster or vector). Finally, it will save 
the masked and aligned raster to an output directory specified by the user (will create the output dir if it does not exist). 

## Requirements
The script requires the ArcPy module to run. It also requires the Spatial Analyst extension from ESRI. 
The script expects 3 arguments from the user:
1. `mask` = feature to mask the rasters by (raster or vector)
2. `snap` = raster file that the other rasters will be aligned with
3. `working_directory` = the working directory where the rasters to be masked are
4. `output_dir` = the directory for output data (can already exist or will be created by script)

## Caveats
Has only been tested with GeoTIFF files, not on rasters inside a geodatabase. Also, it does not have any warning 
messages set up for the user. 

## Uncertainty 
Snapping usually results in a larger output extent than the given extent. The output raster may end up with an 
additional row or column, or both. The cell value in the output raster depends on where the cell center falls on the 
input raster. In this example, the left column and the top row in the output raster ended up being NoData because the 
cell centers of these cells fall outside the input raster. A similar rule is applied when a feature class is used as 
input.


## Acknowledgements 
NA


## References 
https://desktop.arcgis.com/en/arcmap/10.3/tools/environments/how-snap-raster-environment-works.htm
