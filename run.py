#!/usr/bin/env python

"""
Extracts by mask using mask feature and snaps to raster layer to align.
"""

# Import modules
import os
import sys
import getpass
import time
import socket
from datetime import date
import extract_mask_functions as em
import logging

# Usage string.
usage = "<mask feature> <raster to snap to> <working directory> <output directory>"
usageExample = "python " \
               "run.py " \
               "D:\\Projects\\SOG.shp D:\\Projects\\SOG_bathy.tif " \
               "D:\\Projects\\Rasters C:\\Temp\\Output"

# Line break string for log file.
lnbrk = "======================================================================================================="

try:
    if len(sys.argv) != 5:
        # Raise exception for user if they provided incorrent number of arguments.
        print("Invalid number of parameters provided.\n\n" + "Usage: " + usage + "\n\nExample: " + usageExample)
    else:
        # Start timer for interpolation.
        start = time.time()

        # Assign arguments to variables.
        mask = sys.argv[1]
        snap = sys.argv[2]
        working_directory = sys.argv[3]
        output_dir = sys.argv[4]

        out_dir_path = em.change_mk_dir(output_dir)

        log_file_path = os.path.join(out_dir_path, "messages-extract-mask.log")

        print("Messages will be redirected from console to log file here: {}.".format(log_file_path))

        # Redirect messages from console to log file.
        logging.basicConfig(filename=log_file_path,
                            filemode="w",
                            format='%(message)s',
                            level=logging.INFO)
        console = logging.StreamHandler()
        console.setLevel(logging.ERROR)
        logging.getLogger("").addHandler(console)

        # Print details about script run to user.
        logging.info("Script executed on: {} by {} on {}.".format(date.today(), getpass.getuser(), socket.gethostname()))
        logging.info(lnbrk)
        logging.info("Extract by mask: Started")
        logging.info(lnbrk)
        logging.info("Mask feature: {}".format(mask))
        logging.info("Snap raster: {}".format(snap))
        logging.info("Source data directory: {}".format(working_directory))
        logging.info("Output directory: {}".format(out_dir_path))
        logging.info(lnbrk)

        # Execute extract by mask function.
        em.snap_extract(mask, snap, working_directory, out_dir_path)

        # Print
        logging.info(lnbrk)
        logging.info("Extract by mask: Completed")
        logging.info(lnbrk)
        end = time.time()
        hours, rem = divmod(end - start, 3600)
        minutes, seconds = divmod(rem, 60)
        logging.info("Processing time:\n{:0>2}:{:0>2}:{:05.2f}".format(int(hours), int(minutes), seconds))

except Exception as e:
    logging.error(e.message, e.args)
